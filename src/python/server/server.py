from concurrent import futures
import grpc
import logging

from ..config import Config
from ..question_answerer import QuestionAnswerer
from ..generated.question_answering import question_answering_pb2_grpc as pb2_grpc
from ..servicer.question_answering_servicer import QuestionAnsweringServicer

_LOGGER = logging.getLogger()


def serve(config: Config, question_answerer: QuestionAnswerer) -> None:
    """Run server"""

    # Configure server threadpool and add servicer to server.
    server = grpc.server(
        futures.ThreadPoolExecutor(max_workers=config.getint("server", "max_workers"))
    )
    pb2_grpc.add_QuestionAnsweringServicer_to_server(
        QuestionAnsweringServicer(question_answerer), server
    )

    # Add insecure port to server. TODO: Change to secure port (for production)
    server.add_insecure_port(
        f"{config.get('server', 'host')}:{config.get('server', 'port')}"
    )

    # Start server, and block.
    _LOGGER.info("Starting server...")
    server.start()
    _LOGGER.info("Server ready!")
    server.wait_for_termination()
