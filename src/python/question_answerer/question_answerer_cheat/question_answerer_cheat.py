from typing import Tuple

import dateparser  # type: ignore
import grpc
import logging

from ...config import Config
from ...dataset import Dataset
from ...generated.link_prediction import link_prediction_pb2 as lp_pb2
from ...generated.link_prediction import link_prediction_pb2_grpc as lp_pb2_grpc
from ..question_answerer import QuestionAnswerer

_LOGGER = logging.getLogger()


class QuestionAnswererCheat(QuestionAnswerer):
    """
    A 'cheat' implementation of natural language module.
    Cheat meaning simply that it can only process questions
    of a specific format, that we have ourselves generated.
    """

    def __init__(self, dataset: Dataset, config: Config) -> None:
        """Create instance, with dependencies injected."""
        self.dataset = dataset
        self.dataset_name = config.get("dataset", "dataset_name")
        self.tkge_host = config.get("tkge-server", "host")
        self.tkge_port = config.get("tkge-server", "port")

    def answer_question(self, question: str, num_replies: int) -> str:
        _LOGGER.info("Generating query...")
        # Convert question to fact, where either head or tail = -1.
        head, relation, tail, time = self._question_to_query(question)
        _LOGGER.debug(f"Query: ({head}, {relation}, {tail}, {time})")
        lpq = lp_pb2.LinkPredictionQuery(
            head=head,
            relation=relation,
            tail=tail,
            time=time,
            num_replies=num_replies,
            dataset=self.dataset_name,
        )
        _LOGGER.info("Connecting to TKGE server...")
        channel = grpc.insecure_channel(f"{self.tkge_host}:{self.tkge_port}")
        stub = lp_pb2_grpc.LinkPredictionStub(channel=channel)
        _LOGGER.info("Sending query...")
        # Query underlying TKGE or ensemble module
        response = stub.PredictLink(lpq)
        _LOGGER.info("Response received...")
        _LOGGER.debug(f"Response: {response}")
        # Use only first result of n-results. TODO: Show/use more than first result?
        fact = response.facts[0]
        _LOGGER.info("Generating response text...")
        # Generate response text from fact
        answer_id = fact.head if head == -1 else fact.tail
        answer_str = self.dataset.get_entity_str(answer_id)
        _LOGGER.debug(f"Response text: {answer_str}")

        if answer_str is None:
            # Should not be possible? But have to handle optional type.
            raise RuntimeError(
                "String corresponding to answer id was not found in dataset."
            )
        return answer_str

    def _question_to_query(self, question: str) -> Tuple[int, int, int, str]:
        sq = question[:-1].split(" ")
        _LOGGER.debug("Parsing time...")
        dt = dateparser.parse(" ".join(sq[-3:]))
        time = dt.strftime("%d-%m-%Y")
        _LOGGER.debug(f"Time: {time}")

        # If "Who did", missing tail.
        if "Who did" in question:
            _LOGGER.debug("'Who did' in question, searching for head and relation...")
            sq = sq[2:-4]
            # Loop through words, untill found in dataset
            for i in range(1, len(sq)):
                ent_id = self.dataset.get_entity_id(" ".join(sq[:i]))
                if ent_id is not None:
                    entity = " ".join(sq[:i])
                    _LOGGER.debug(f"Head found: {entity}")
                    relation = " ".join(sq[i:])
                    _LOGGER.debug(f"Relation found: {relation}")
                    is_tail = True
                    break
        # If not "Who did", then missing head.
        else:
            _LOGGER.debug(
                "'Who did' not in question, searching for tail and relation..."
            )
            sq = sq[1:-4]
            for i in range(len(sq) - 1, 0, -1):
                ent_id = self.dataset.get_entity_id(" ".join(sq[i:]))
                if ent_id is not None:
                    entity = " ".join(sq[i:])
                    _LOGGER.debug(f"Tail found: {entity}")
                    relation = " ".join(sq[:i])
                    _LOGGER.debug(f"Relation found: {relation}")
                    is_tail = False
                    break

        entity_id = self.dataset.get_entity_id(entity)
        # Relations start with capital letter in dataset
        relation_id = self.dataset.get_relation_id(relation[0].upper() + relation[1:])
        _LOGGER.debug(f"Entity and relation IDs: {entity_id}, {relation_id}")
        head = entity_id if is_tail else -1
        tail = -1 if is_tail else entity_id

        if None in [head, relation_id, tail]:
            raise RuntimeError(
                "Some part of head, relation, tail was not found in dataset."
            )
        return head, relation_id, tail, time  # type: ignore
