from abc import ABC, abstractmethod


class QuestionAnswerer(ABC):
    @abstractmethod
    def answer_question(self, question: str, num_replies: int) -> str:
        """Answer a natural language question.
        Consists of converting this question into a LinkPredictionQuery,
        sending this to underlying TKGE or Ensemble module,
        and receiving and returning response.

        Args:
            question (str): Natural language question to answer.

            num_replies (int): Number of replies requested.
            Useful when TKGE models are not perfect,
            and Hits@1 might be low, but Hits@10 high.

        Returns:
            str: A natural language response, to the natural language question.
        """
        raise NotImplementedError
