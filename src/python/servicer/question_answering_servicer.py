import logging

from ..question_answerer import QuestionAnswerer
from ..generated.question_answering import question_answering_pb2 as qa_pb2
from ..generated.question_answering import question_answering_pb2_grpc as qa_pb2_grpc

_LOGGER = logging.getLogger()


class QuestionAnsweringServicer(qa_pb2_grpc.QuestionAnsweringServicer):
    """
    Implementation of generated QuestionAnsweringServicer.
    Instantiate this, and register it using gRPC.
    """

    def __init__(self, question_answerer: QuestionAnswerer) -> None:
        """
        Create istance of QuestionAnsweringServicer, using dependency injection
        for question answering implementation.

        Args:
            question_answerer (QuestionAnswerer): Question answering implementation to use.
        """
        super().__init__()
        self.question_answerer = question_answerer

    def AnswerQuestion(self, request, context):
        """Answer question by converting to query, sending to TKGE and recieving response."""
        try:
            _LOGGER.info("Request received...")
            _LOGGER.debug(f"Request: {request}")
            answer = self.question_answerer.answer_question(
                request.question, request.num_answers
            )
            _LOGGER.info("Response generated...")
            _LOGGER.debug(f"Response: {answer}")
            awc = qa_pb2.AnswerWithConfidence(answer=answer, confidence=1.0)
            _LOGGER.info("Returning answer with confidence...")
            _LOGGER.debug(f"AWC: {awc}")
            return qa_pb2.Answer(answer_with_confidence=[awc])
        except Exception as e:
            _LOGGER.warning(f"Could no process request. Error message: {e}")
