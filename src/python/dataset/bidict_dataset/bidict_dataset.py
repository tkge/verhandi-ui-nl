import csv
import logging
from bidict import bidict
from pathlib import Path
from typing import Optional


from ..dataset import Dataset

_LOGGER = logging.getLogger()


class BidictDataset(Dataset):
    def __init__(self, path: str) -> None:
        _LOGGER.info("Generating dataset...")
        self._entity_dict: bidict[str, int] = bidict()
        self._relation_dict: bidict[str, int] = bidict()

        entity_path = Path(path) / "entity2id.txt"
        relation_path = Path(path) / "relation2id.txt"
        _LOGGER.debug(f"Entity ID path: {entity_path}")
        _LOGGER.debug(f"Relation ID path: {relation_path}")

        _LOGGER.debug("Reading Entity ID file...")
        with open(entity_path, "r") as entity_file:
            reader = csv.reader(entity_file, delimiter="\t")
            for row in reader:
                self._entity_dict[row[0]] = int(row[1])

        _LOGGER.debug("Reading Relation ID file...")
        with open(relation_path, "r") as relation_file:
            reader = csv.reader(relation_file, delimiter="\t")
            for row in reader:
                self._relation_dict[row[0]] = int(row[1])

    def get_entity_id(
        self, entity_str: str, default: Optional[int] = None
    ) -> Optional[int]:
        if default is not None:
            return self._entity_dict.get(entity_str, default)
        return self._entity_dict.get(entity_str)

    def get_entity_str(
        self, entity_id: int, default: Optional[str] = None
    ) -> Optional[str]:
        if default is not None:
            return self._entity_dict.inverse.get(entity_id, default)
        return self._entity_dict.inverse.get(entity_id)

    def get_relation_id(
        self, relation_str: str, default: Optional[int] = None
    ) -> Optional[int]:
        if default is not None:
            return self._relation_dict.get(relation_str, default)
        return self._relation_dict.get(relation_str)

    def get_relation_str(
        self, relation_id: int, default: Optional[str] = None
    ) -> Optional[str]:
        if default is not None:
            return self._relation_dict.inverse.get(relation_id, default)
        return self._relation_dict.inverse.get(relation_id)
