from abc import ABC, abstractmethod
from typing import Optional


class Dataset(ABC):
    """Class for accessing KG datasets."""

    @abstractmethod
    def get_entity_id(
        self, entity_str: str, default: Optional[int] = None
    ) -> Optional[int]:
        raise NotImplementedError

    @abstractmethod
    def get_entity_str(
        self, entity_id: int, default: Optional[str] = None
    ) -> Optional[str]:
        raise NotImplementedError

    @abstractmethod
    def get_relation_id(
        self, relation_str: str, default: Optional[int] = None
    ) -> Optional[int]:
        raise NotImplementedError

    @abstractmethod
    def get_relation_str(
        self, relation_id: int, default: Optional[str] = None
    ) -> Optional[str]:
        raise NotImplementedError
