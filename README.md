# Natural Language to Query Module

UI and NL modules of `Verðandi`

## Installation

Install requirements using [poetry](https://python-poetry.org/).

```bash
poetry install
```

or [pip](https://pip.pypa.io/en/stable/)

```bash
pip install -r requirements.txt --no-deps
```

## Usage
Enter desired hostname, port and number of workers for this server, into `config.ini` under section `[server]`.
Also enter hostname and port of TKGE module or Ensemble module, under section `[tkge_server]`.

Run using `main.py`. Should be used in tandem with [Verðandi Ensemble module](https://gitlab.com/tkge/verhandi-tkge-module-ensemble) and [Verðandi TKGE modules](https://gitlab.com/tkge/verdandi-tkge-modules-de).

`client.py` script can be used to ask questions to the system, which will then be passed through the whole system of (optional) Ensemble Module and TKGE module(s), and return a natural language response. Use `client.py "{question}"`.

## Development

To include a new method of converting natural language to queries, create a new implementation of `QuestionAnswerer`, where method `answer_question` takes a natural language question as a string, and returns a natural language response as a string.

See `question_answerer_cheat.py` for inspiration.


## License
[MIT](https://choosealicense.com/licenses/mit/)
