import logging
import logging.config
import logging.handlers
from argparse import ArgumentParser

from src.python.question_answerer import QuestionAnswererCheat
from src.python.config import Config
from src.python.dataset import BidictDataset
from src.python.server import serve

# TODO: add command line args for overriding config.ini


def _generate_arg_parser() -> ArgumentParser:
    parser = ArgumentParser(description="Run NL2Q server.")
    # Server
    parser.add_argument(
        "--host", type=str, dest="server.host", help="This server hostname."
    )
    parser.add_argument(
        "--port", type=str, dest="server.port", help="This server port."
    )
    parser.add_argument(
        "--max-workers",
        type=str,
        dest="server.max_workers",
        help="This server max workers.",
    )
    # TKGE server
    parser.add_argument(
        "--tkge-host", type=str, dest="tkge-server.host", help="TKGE server hostname."
    )
    parser.add_argument(
        "--tkge-port", type=str, dest="tkge-server.port", help="TKGE server port."
    )
    # Dataset
    parser.add_argument(
        "--dataset-name", type=str, dest="dataset.dataset_name", help="Dataset name."
    )
    parser.add_argument(
        "--dataset-path", type=str, dest="dataset.dataset_path", help="Dataset path."
    )
    # Logging
    parser.add_argument(
        "--logging-level", type=str, dest="logging.level", help="Logging level."
    )
    parser.add_argument(
        "--logging-output-path",
        type=str,
        dest="logging.output_path",
        help="Logging file output path (Folder).",
    )
    parser.add_argument(
        "--logging-write-to-file",
        type=str,
        dest="logging.write_to_file",
        help="Whether to log to file.",
    )
    return parser


if __name__ == "__main__":
    logging.config.fileConfig("logging.conf")
    parser = _generate_arg_parser()
    args = parser.parse_args()
    config = Config(**vars(args))
    dataset = BidictDataset(config.get("dataset", "dataset_path"))  # type: ignore
    question_answerer = QuestionAnswererCheat(dataset, config)

    serve(config, question_answerer)
