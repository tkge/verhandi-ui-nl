import grpc
import argparse

import src.python.generated.question_answering.question_answering_pb2 as pb2
import src.python.generated.question_answering.question_answering_pb2_grpc as pb2_grpc


def client(question: str) -> None:
    channel = grpc.insecure_channel("localhost:50056")
    stub = pb2_grpc.QuestionAnsweringStub(channel=channel)
    question = pb2.Question(question=question, num_answers=10)
    response = stub.AnswerQuestion(question)
    print(response)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("question", help="question to ask")
    args = parser.parse_args()
    client(args.question)
